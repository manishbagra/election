<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ChefController@index');

Route::get('/step-1', function () {return view('step-1');});
	
Route::post('/second_step_reg', 'ChefController@storeFirstStep');

Route::get('/step-2', 'ChefController@viewStepSecond');

Route::get('/submit_info_with_check_otp' , 'ChefController@storeFormDetails');

Route::get('/unset_otp', 'ChefController@unsetOtp');

Route::get('/step-3/{id}', 'ChefController@viewStepThree');

Route::post('/store_election_type', 'ChefController@storeElectionType');

Route::get('/step-4', 'ChefController@storeLocation');

Route::post('/get_states', 'ChefController@getStatesByCountryId');

Route::post('/get_cities', 'ChefController@getCityByStateId');

Route::post('/getLocationsDetails', 'ChefController@uploadDocPrivew');

Route::get('/step-5', 'ChefController@readyTODoc');

Route::get('/step-6', function () {return view('step-6');});

Route::get('/step-7', function () {return view('step-7');});

Route::post('move_uploaded_excel_csv_file',"ChefController@uploadedDocInFolder");

Route::post('check_convert_doc',array('as'=>'excel.import','uses'=>'ChefController@importExportExcelORCSV'));

Route::get('/people', 'chefController@peopleList');

Route::get('/tellers', 'chefController@tellerlist');

Route::POST('/insert_teller', 'chefController@telleradd');

Route::get('/teller_panel/{id}', 'chefController@tellershow');

Route::POST('/remove_teller', 'chefController@removeTeller');

Route::POST('/update_people', 'chefController@savePeople');

//Route::POST('/remove', 'ChefController@remove');

Route::POST('/check_register', 'chefController@registerpeople');

Route::get('/ballots', 'chefController@ballots');

Route::POST('/getPeopleLoad', 'chefController@getPeopleLoad');

Route::POST('/insert_ballot', 'chefController@addballot');


Route::get('autocompleted', 'chefController@autocompletes');

Route::POST('/listballot', 'chefController@ballotlist');

Route::POST('/delete_ballot', 'chefController@deleteballot');	

Route::POST('/edit_ballot', 'chefController@editballot');


Route::POST('/editinsert_ballot', 'chefController@editinsertballot');

Route::get('/results', 'chefController@result');

Route::get('/finalresult', 'chefController@finalresult');

Route::get('/teller_panel/ballots', 'chefController@tellerballot');


Route::get('/ballotdone', 'chefController@ballotsdone');

Auth::routes();

Route::group(['middleware' => ['auth']], function(){});

