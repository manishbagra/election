<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => true,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAtchhQr8:APA91bFpUdjkWUydx8YvEzg9udwofCD2fLv3f8QB8xdB7baYvqEarq6782_TrrvRda6ss-VJhK6UuZa8J-GT5vHQxp6eZiV6FxRsXIO76OVLTGBUnaacivPYDglq9iLpxgg8HQLWzBFb'),
        'sender_id' => env('FCM_SENDER_ID', '780750897855'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
