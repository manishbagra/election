<?php

namespace App\Helpers;

use App\Business;
use App\BusinessAddress;
use App\BusinessPlanRate;
use App\BusinessSupport;
use App\SubscriptionStripe;
use App\BusinessPlanMapping;
use Auth;
use DB;

class Helper
{
	
   	public static function getAdminUserId()
    {
		return Auth::user()->id;
	}
	
	public static function getAdminUserType()
    {
		return Auth::user()->role_id;
	}
	
	public static function getAdminUserName()
    {
		$email = Auth::user()->email;
		
		return $email;
	}
	
	public static function getRS_Provider()
    {
		$rsProviderArray = array(1 => 'Uber', 2 => 'Lyft', 3 => 'Both (Uber & Lyft)', 4 => 'Other');
		return $rsProviderArray;
	}
	
	public static function getLevel_Provider()
    {
		$levelProviderArray = array(1 => 'X', 2 => 'XL', 3 => 'Pool', 4 => 'Black', 5 => 'Select', 6 => 'Lyft', 7 => 'LyftPlus', 8 => 'Line', 9 => 'Premier', 10 => 'Other');
		return $levelProviderArray;
	}
	
	public static function getUniqueId()
	{
		return $ImageName = str_replace(".", "", microtime(date('Y-m-d H:i:s')));		
	}
		
	public static function getImageFolderPath()
    {
        return "http://app.ridealong.media";
    }
	
	public static function getImageRootPath()
    {
        return "/var/www/html";
    }
	
	public static function setUrlName($string){
		$UrlName = preg_replace("/[^A-Za-z0-9 \-]/", '', $string);
		$ExistString   = array("' and '", "' - '", "'[\s]+'", "'----'", "'---'", "'--'", "'/'");
		$ReplaceString = array("-", "-", "-", "-", "-", "-", "-");
		$UrlName = preg_replace($ExistString, $ReplaceString, $UrlName);
		return strtolower($UrlName);
	}
	
	public static function setImgName($string){
		$ImageName = preg_replace("/[^A-Za-z0-9 \-]/", '', $string);
		$ExistString   = array("' and '", "' - '", "'[\s]+'", "'----'", "'---'", "'--'", "'/'");
		$ReplaceString = array("-", "-", "-", "-", "-", "-", "-");
		$ImageName = preg_replace($ExistString, $ReplaceString, $ImageName);
		return strtolower($ImageName);
	}
	
	public static function removeImg($string){
		if(file_exists($string)){
			@unlink($string);
		}
	}
	
	public static function getsupportNotification()
	{
		$Qry = BusinessSupport::where("status", 1)->get();
		return count($Qry);
	}
	
	public static function getcartNotification()
	{
		$UserId = Helper::getAdminUserId();
		$BusinessRst = Business::where("user_id", $UserId)->first();
		$businessId  = $BusinessRst->bid;
		
		$folioRst = DB::select("SELECT BF.*, BPA.addons_name
								FROM business_folio as BF INNER JOIN business_plan_addons as BPA ON BPA.addons_id = BF.addons_id
								WHERE BF.bid = ".$businessId." AND BF.expired_at = '0000-00-00'");
		$folioNum = count($folioRst);
		return $folioNum;
	}
	
	public static function getUnlockedProfileStatss($businessId)
	{
		$TotalLocation = BusinessAddress::where('bid', $businessId)->count();
		$planId   = Helper::getPlanId($TotalLocation);
		$addonsId = 1;
		$folioOBJ = DB::select("SELECT *
								FROM business_folio WHERE bid = ".$businessId." AND plan_id = ".$planId." AND addons_id = ".$addonsId."
								AND expired_at >= CAST(NOW() as date)");
		return $folioNum = count($folioOBJ);
	}
	
	public static function getUnlockedProfileStat($businessId)
	{
		$subscriptionOBJ = SubscriptionStripe::where("bid", $businessId)->first();
		$subscriptionCNT = count($subscriptionOBJ);
		
		\Stripe\Stripe::setApiKey('sk_live_yx0rjNPaOok7kF5gzksxnLhQ');
		if($subscriptionCNT>0)
		{
			$subscribeId = $subscriptionOBJ->stripe_subs_id;
			$subscription = \Stripe\Subscription::retrieve($subscribeId);
			$StripePlanId = $subscription->plan->id;
			
			$mapRst = BusinessPlanMapping::where('stripe_id', $StripePlanId)->first();
			$AddonsId = $mapRst->addons_id;
			
		}else{
			$AddonsId = 0;
		}
		return $AddonsId;
	}
	public static function getPlanId($TotalLocation)
	{
		if($TotalLocation==1){
			$plan_id = 1;
		}elseif($TotalLocation>=2 && $TotalLocation <=5){
			$plan_id = 2;
		}elseif($TotalLocation>=6 && $TotalLocation <=15){
			$plan_id = 3;
		}elseif($TotalLocation>=16){
			$plan_id = 4;
		}
		return $plan_id;
	}
	
	public static function get_instagram($userUrl,$count=12,$width=125,$height=125)
	{
		$url = $userUrl.'&count='.$count;
	
		// Also Perhaps you should cache the results as the instagram API is slow
		$cache = './'.sha1($url).'.json';
		if(file_exists($cache) && filemtime($cache) > time() - 60*60){
			// If a cache file exists, and it is newer than 1 hour, use it
			$jsonData = json_decode(file_get_contents($cache));
		} else {
			$jsonData = json_decode((file_get_contents($url)));
			file_put_contents($cache,json_encode($jsonData));
		}
	
		$result = '<div id="instagram">'.PHP_EOL;
		foreach ($jsonData->data as $key=>$value) {
			$result .= "\t".'<a class="fancybox" data-fancybox-group="gallery" 
								title="'.htmlentities($value->caption->text).' '.htmlentities(date("F j, Y, g:i a", $value->caption->created_time)).'"
								style="padding:3px" href="'.$value->images->standard_resolution->url.'">
							  <img src="'.$value->images->low_resolution->url.'" alt="'.$value->caption->text.'" width="'.$width.'" height="'.$height.'" style="margin-bottom:10px;" />
							  </a>'.PHP_EOL;
		}
		$result .= '</div>'.PHP_EOL;
		return $result;
	}
}