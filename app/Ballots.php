<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ballots extends Model
{
    protected $table      = 'ballots';
	
	protected $primaryKey = 'id';
	
	protected $fillable   = ['ballot_no', 'user_id', 'created_on'];

}
