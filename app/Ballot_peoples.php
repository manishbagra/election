<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ballot_peoples extends Model
{
    protected $table      = 'ballot_peoples';
	
	protected $primaryKey = 'id';
	
	protected $fillable   = ['ballot_id', 'name', 'created_on'];

}
