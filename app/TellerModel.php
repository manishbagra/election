<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TellerModel extends Model
{
  
    protected $table      = 'users';
	
	protected $primaryKey = 'id';
	
	protected $fillable   = ['email', 'roles', 'parents_id', 'code', 'phone', 'e_type', 'country_id', 'state_id', 'city_id', 'created_on','updated_on','active'];

   
}
