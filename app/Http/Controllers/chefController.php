<?php
namespace App\Http\Controllers;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Chef;
use App\CountryModel;
use App\StateModel;
use App\CityModel;
use App\PeopleModel;
use App\TellerModel;
use App\Ballots;
use App\Ballot_peoples;
use DB;
use Validator;
use session;
use Helper;
use Hash;

class chefController extends Controller
{
	public function __construct(UrlGenerator $url)
    {
        //$this->middleware('auth');
		$this->url = $url;
    }
	
	public function index(){
		//echo base64_encode("Um19rcneJi");
		return view('step-1');
		
	}

	public function storeFirstStep(Request $request)
	{	
		$validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
			'phone' => 'required|numeric',
			
        ]);
		
		if ($validator->fails()) {
             
            return redirect('/')->withErrors($validator);
        }
		else 
		{
			$time = time();
		   
			$data['email'] = $request->email;
			$data['code']  = $request->code;
			$data['phone'] = $request->phone;
			$data['Time']  = $time;
			
			$digits = 4;
			
			$data['otp'] = rand(pow(10, $digits-1), pow(10, $digits)-1);
			
			\Session::put("data" , $data);
			
			return redirect()->to('step-2');
		}
	}
	
	public function viewStepSecond(Request $request)
	{	
		if (Session('data.otp') != "") {
			
			return view('step-2');
		
		}else{
			
			return redirect()->to('step-1');
			
		}
		
	}
	
	public function generateRandomString($length = 10) {
		
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
		
	}
	
	public function storeFormDetails()
	{	 
		$otp = $_GET['otp'];
		
		$otp_sess = "";
		
		if(!empty($otp)){
			
			$otp_sess = Session('data.otp');
			
			if($otp_sess == $otp){
				
				$date  		= date('Y-m-d H:i:s');
				$email 		= Session('data.email');
				$code  		= Session('data.code');
				$phone 		= Session('data.phone');
				$uniqueId 	= base64_encode($this->generateRandomString());
				
				$roles = 1 ;
				$id = Chef::insertGetId([
				'unique_id'  => base64_decode($uniqueId),
				'email' 	 => $email, 
				'code'	 	 => $code , 
				'phone' 	 => $phone,
				'roles' 	 => $roles , 
				'created_on' => $date
				]);
				     
				if($id){
					
					Session(['data' => ""]);
					
					return response()->json(['status' => true , 'response' =>'Saved Data' , 'id' => $uniqueId]);
					
				}else{
					return response()->json(['status' => false , 'response' =>'Data Not inserted']);
				}
				
			}else{
				
				return response()->json(['status' => false , 'response' =>'OTP Expired']);
			}
			
		}
	}
	
	
	public function unsetOtp()
	{	
		$otp = $_GET['otp'];
		
		Session(['data.otp' => ""]);
		
		return response()->json(['status' => true, 'response' =>'Working']);
	}
	
	public function viewStepThree($id)
	{
		if (base64_encode(base64_decode($id, true)) === $id){
			
			$bid = base64_decode($id);
		
			$checkExistUser = Chef::where('unique_id',$bid);
		
			$result = $checkExistUser->get();
		
		
			if($checkExistUser->count()){
			
			if($result[0]['active']==1){
				
				if (Session('user') != ""){
					Session(['user' => ""]);
				}	
				
				
				$flag = 1 ;
				
			}
			else{
			
				$flag = 0 ;
				
			}
			
			//echo '<pre>'; print_r($result[0]);die;
			
			\Session::put("user" , $result[0]);
			
			
			return view('step-3')->with('flag',$flag);
			
			}else{
			
			\Session::put("user" , '');
			
			return redirect()->to('step-1');
			}
			
		}else{
			
			return redirect()->to('step-1');
			
		}
		
	}
	
	public function storeElectionType(Request $request)
	{
		$date  = date('Y-m-d H:i:s');
		$data['type']    = $request->radio;
		$data['user_id'] = Session('user.id');
		
		$type = DB::table('users')
            ->where('id', $data['user_id'])
            ->update(['e_type' => $data['type'] ,'updated_on' => $date,'active' => 1]);
			
		return redirect()->to('step-4');
		
	}
	
	public function storeLocation()
	{	
	    $countries = CountryModel::orderBy('name')->where('id',13)->get();
		$all_state = StateModel::orderBy('name')->where('country_id',13)->get();
		
		return view('step-4')->with('countries' , $countries)->with('all_state' , $all_state);
	}
	
	public function getCityByStateId(Request $request)
	{	
	
	  $date  = date('Y-m-d H:i:s');
	 
	 
	  $state_id = $request->state_id ;
	  
	 
	  $all_city = CityModel::orderBy('id')->where('state_id',$state_id)->get();
	    
		if(count($all_city) > 0 ){
		  
			foreach($all_city as $element) { echo "<option value='$element->id'>$element->name</option>"; }
		  
		}else{
			 echo "<option  value=''><span style='color:red;' >No city in this state</span></option>";
		} 
		$user_id = Session('user.id');
	    $type = DB::table('users')
            ->where('id', $user_id)
            ->update(['state_id' => $state_id,'city_id' => $element->id,'updated_on' => $date]);
	}
	
	public function uploadDocPrivew(Request $request)
	{
		return redirect()->to('step-5');
	}
	
	public function readyTODoc(Request $request)
	{
		return view('step-5');
	}
	
	public function uploadedDocInFolder(Request $request)
	{
		$user_id = Session('user.id');
		$date  = date('Y-m-d H:i:s');
		$name = $_FILES['doc_file']['name'];
		
		$temp = $_FILES['doc_file']['tmp_name'];
		$filename = DB::table('users')
            ->where('id', $user_id)
            ->update(['file_name' => $name,'updated_on' => $date]);
		$src  = "uploads/csv_docs/".$name;
		 
		if(file_exists($src)){
			
			$json_array = array('status' => false, 'path_file' => '');
			
		}else{ 
		     
			move_uploaded_file($temp,$src);
			
			$json_array = array('status' => true, 'path_file' => $src);
		}	
		
		echo json_encode($json_array);exit();
		
	}
	
	
	public function importExportExcelORCSV(Request $request)
	{ 
	
			$id = Session('user.id');
				
				$data = \Excel::load($request->path)->get();
				
				if($data->count()){
					
					$date  = date('Y-m-d H:i:s');
					
					foreach ($data[0] as $key => $value) {
						
						$arr[] = ['user_id' => $id,'surename' => $value->surename, 'lastname' => $value->lastname, 'baha_id' => $value->baha_id , 'community' => $value->community, 'created_on' => $date];
						
					}
					
					if(!empty($arr)){
						
						PeopleModel::insert($arr);
						
						$json_array = array('status' => true, 'success' => 1);
						
					}else{
						
						$json_array = array('status' => false, 'error' => 1);
						
					}
				}
         
	
		echo json_encode($json_array);exit();
		
	}
	
	
	public function peopleList(Request $request)
    {	
	   
	   
		$allPeople = PeopleModel::orderBy('people_id')->where('user_id',Session('user.id'))->paginate(10);
		
		$dataArray = array(
		'uri'  		=> 'people',
		'flag' 		=> 0,
		'allPeople' => $allPeople,
		'count' 	=> PeopleModel::where('user_id',Session('user.id'))->count(),
		'filename'  => Chef::select('file_name' )->where('id',Session('user.id'))->get(),
		'registerpeople' 	=> PeopleModel::orderBy('people_id')->where('registered',1 )->where('user_id',Session('user.id'))->count(),
		'notregisterpeople' 	=> PeopleModel::orderBy('people_id')->where('registered',0 )->where('user_id',Session('user.id'))->count(),
		'votintype' 	=> PeopleModel::orderBy('people_id')->where('can_vote',1 )->where('user_id',Session('user.id'))->count(),
		'PSJ' 		=> ($request->input('page', 1) - 1) * 10 
		);
		
		return view('people')->with($dataArray);
		
	}	
   
	public function tellerlist(Request $request)
    {
		$showtellers = DB::table('users')->where('parents_id',Session('user.id'))->get();
		
		$dataArray = array(
		'flag' 			=> 0,
		'count'			=> $showtellers->count(),
		'showtellers' 	=> $showtellers,
		'baseurl' 		=> $this->url->to('/'),
		'uri' 			=> 'tellers');
		
		return view('tellers')->with($dataArray);
		
	}	
	
	public function telleradd(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
			'phone' => 'required|numeric',
			
        ]);
		
		if ($validator->fails()) {
			//print_r('aaa');die;
            return redirect('tellers')->withErrors($validator);
        }
		else 
		{
				$flag = 0 ;
				
				$date  = date('Y-m-d H:i:s');
				
				$uniqueId 	= base64_encode($this->generateRandomString());
				
				$id = Chef::insertGetId([
				'unique_id'  => base64_decode($uniqueId),
				'email' 	 => $request['email'],
				'roles' 	 => 2,
				'parents_id' => Session('user.id'),
				'code' 		 => $request['code'] ,
				'phone' 	 => $request['phone'],
				'created_on' => $date]);
		}
	
	}
	
	public function tellershow($id,Request $request)
	{
		
		if (base64_encode(base64_decode($id, true)) === $id){
			
			$allPeople = PeopleModel::where('user_id',Session('user.id'))->paginate(10);
			
			$showtellers = Chef::where('unique_id', base64_decode($id))->get();
			
			$dataArray = array(
			'uri'  		 => 'people',
			'flag' 		 => 1,
			'teller' 	 => $showtellers[0],
			'filename'  => Chef::select('file_name' )->where('id',Session('user.id'))->get(),
			'allPeople'  => $allPeople,
			'count' 	 => PeopleModel::where('user_id',Session('user.id'))->count(),
			'registerpeople' 	=> PeopleModel::orderBy('people_id')->where('registered',1 )->where('user_id',Session('user.id'))->count(),
		   'notregisterpeople' 	=> PeopleModel::orderBy('people_id')->where('registered',0 )->where('user_id',Session('user.id'))->count(),
		    'votintype' 	=> PeopleModel::orderBy('people_id')->where('can_vote',1 )->where('user_id',Session('user.id'))->count(),
			'PSJ' 		 => ($request->input('page', 1) - 1) * 10
			);
				//echo'<pre>';print_r($dataArray .flag);die;
			return view('people')->with($dataArray);
	    
		}else {
			print_r(url('/teller_panel/ballots'));die;
			//return redirect()->to('step-1');
		}
	}
	
	public function removeTeller(Request $request)
	{
		
		$teller_id = $request->teller_id ;
		
		$chef_id   = $request->chef_id ;
		
		$result = Chef::where('id',$teller_id)->where('parents_id',$chef_id)->delete();
		
		if($result > 0){
			$json_array = array('status'=> true);
			
		}else{
			$json_array = array('status'=> false);
		}
		
		echo json_encode($json_array);exit();
		
	}
	
	public function savePeople(Request $request)
	{	
	   print_r($request->all());
		$box     = $request->all();  
		
		$myValue =  array();
		
		parse_str($box['form_input'], $myValue);
		
		$people_id 		= $myValue['id'];
		
		$register = 0;
		
		if(@$myValue['register']){
			$register = $myValue['register'];
		}
		
		$voting 	= $myValue['voting'];
		
		if(@$myValue['voting']){
			$voting = $myValue['voting'];
		}
		
		$can_vote 	= 0;
		
		if(@$myValue['can_vote']){
			$can_vote = $myValue['can_vote'];
		}
		
		$res = DB::table('people')
            ->where('people_id', $people_id)
            ->update(['registered'=>$register,'voting_type'=>$voting,'can_vote'=>$can_vote]);
		
		if($res == 0){
			echo json_encode(array('status' => true)); exit();
		}else{
			echo json_encode(array('status' => false)); exit();
		}
		
	}
	public function registerpeople(Request $request)
	{
		
		$limit1 = 0;
		
		$limit2 = 10;

		
		
		switch ($request->radiovalue) {
				case 1:
				 
					$registerpeople = PeopleModel::orderBy('people_id')->where('user_id',Session('user.id'))->skip($limit1)->take($limit2)->get();
					
					$count = PeopleModel::orderBy('people_id')->where('user_id',Session('user.id'))->count();
					
					break;
				case 2:
				
					$registerpeople = PeopleModel::orderBy('people_id')->where('registered',1 )->where('user_id',Session('user.id'))->skip($limit1)->take($limit2)->get();
					
					$count = PeopleModel::orderBy('people_id')->where('registered',1 )->where('user_id',Session('user.id'))->count();
					
					break;
				case 3:
					
					$registerpeople = PeopleModel::orderBy('people_id')->where('registered',0)->where('user_id',Session('user.id'))->skip($limit1)->take($limit2)->get();
					$count = PeopleModel::orderBy('people_id')->where('registered',0 )->where('user_id',Session('user.id'))->count();
					break;
				case 4:
				
					$registerpeople = PeopleModel::orderBy('people_id')->where('can_vote',1)->where('user_id',Session('user.id'))->skip($limit1)->take($limit2)->get();
					$count = PeopleModel::orderBy('people_id')->where('can_vote',1 )->where('user_id',Session('user.id'))->count();
					break;
			}
        
       
		$jsonArray = array(
		'status' => true,
		'count'  => $count,
		'type'   => $request->radiovalue,
		'dataArray' =>$registerpeople,
		'flag' 		 => 0);
		echo json_encode($jsonArray); exit();
		
			
	}
	public function getPeopleLoad(Request $request)
	{
		//echo '<pre>'; print_r($_POST);die;
		
		if(!empty($request->type) &&  !empty($request->count) &&  !empty($request->val)){
			
			//echo '<pre>'; print_r($_POST);die;
			
			
			
			if($request->val == 2){
				
				$limit1 = $request->count - 1 ;
				
				$limit2 = 10;
			}
			if($request->val == 1){
				
				$limit1 = $request->count - 1 ;
				
				$limit2 = 10;
			}
			
			switch ($request->type) {
				
				case 1:
				 
					$registerpeople = PeopleModel::orderBy('people_id')->where('user_id',Session('user.id'))->skip($limit1)->take($limit2)->get();
					
					$count = PeopleModel::orderBy('people_id')->where('user_id',Session('user.id'))->count();
					
					break;
				case 2:
				
					$registerpeople = PeopleModel::orderBy('people_id')->where('registered',1 )->where('user_id',Session('user.id'))->skip($limit1)->take($limit2)->get();
					
					$count = PeopleModel::orderBy('people_id')->where('registered',1 )->where('user_id',Session('user.id'))->count();
					
					break;
				case 3:
					
					$registerpeople = PeopleModel::orderBy('people_id')->where('registered',0)->where('user_id',Session('user.id'))->skip($limit1)->take($limit2)->get();
					$count = PeopleModel::orderBy('people_id')->where('registered',0 )->where('user_id',Session('user.id'))->count();
					break;
				case 4:
				
					$registerpeople = PeopleModel::orderBy('people_id')->where('can_vote',1)->where('user_id',Session('user.id'))->skip($limit1)->take($limit2)->get();
					$count = PeopleModel::orderBy('people_id')->where('can_vote',1 )->where('user_id',Session('user.id'))->count();
					break;
			}
			
			$jsonArray = array(
			'status' => true,
			'count'  => $count,
			'countInc' => $request->count,
			'type'   => $request->type,
			'dataArray' =>$registerpeople,
			'flag' 		 => 0);
			
		
			
		}else{
			$jsonArray = array('status' => false, 'response' => 'something wrong');
		}
		
		echo json_encode($jsonArray); exit();
	}
	
   public function ballots(Request $request){
	   $Ballot_no = ballots::select('id','ballot_no' )->where('user_id',Session('user.id'))->get();
	  
	  $count = ballots::where('user_id',Session('user.id'))->count();
	  $i='';
	  $ballot = array();

	  for($i=0;$i<$count;$i++)
	   {
		$ballot[] = ballot_peoples::select('name' )->where('ballot_id',$Ballot_no[$i]->id)->get();   
	   }
	   
	    
	 //echo'<pre>'; print_r($ballot);die;
	  
	  
	  $jsonArray = array(
		'uri'  		 => 'ballots',
		'ballot'  => $ballot,
		'Ballot_no'  => $Ballot_no,
		'count'  => $count,
		'flag' 		 => 0);
		
		//echo'<pre>';print_r($jsonArray->Ballot_no);die;
		
		return view('ballots')->with($jsonArray);
	   
	}
	public function addballot(Request $request)
	{	
	      	
				$date  = date('Y-m-d H:i:s');
				
				$id = Ballots::insertGetId([
				'ballot_no' 	 => $request['ballot_no'],
				'user_id' 	 => Session('user.id'),
				'created_on' => $date]);
				$data = array(
                  array('ballot_id'=>$id, 'name'=> $request['ballot1'],'updated_on'=> $date),
                  array('ballot_id'=>$id, 'name'=> $request['ballot2'],'updated_on'=> $date),
				  array('ballot_id'=>$id, 'name'=> $request['ballot3'],'updated_on'=> $date),
				  array('ballot_id'=>$id, 'name'=> $request['ballot4'],'updated_on'=> $date),
				  array('ballot_id'=>$id, 'name'=> $request['ballot5'],'updated_on'=> $date),
				  array('ballot_id'=>$id, 'name'=> $request['ballot6'],'updated_on'=> $date),
				  array('ballot_id'=>$id, 'name'=> $request['ballot7'],'updated_on'=> $date),
				  array('ballot_id'=>$id, 'name'=> $request['ballot8'],'updated_on'=> $date),
				  array('ballot_id'=>$id, 'name'=> $request['ballot9'],'updated_on'=> $date),
				 );
		       
               DB::table('ballot_peoples')->insert($data);
			   $jsonArray = array(
				'status' => true,
				'flag' 		 => 0);
			echo json_encode($jsonArray); exit();
	}
	 public function autocompletes (Request $request)
		{
			
			$term = $request->terms;
          
			$queries = DB::table('people')->where('surename', 'like', '%'.$term.'%')->where('user_id',Session('user.id'))->take(9)->get(); //Your table name
			$results=array();
			foreach ($queries as $query)
			{
				$results[] = ['id' => $query->people_id, 'value' => $query->surename];
			    
			}
		
		
		echo json_encode($results); exit(); 
		}
		public function ballotlist(Request $request)
		 {
			 //print_r($request->radiovalue);die;
			 switch ($request->radiovalue) {
				case 1:
				 
					 $Ballot_no = ballots::select('id','ballot_no' )->where('user_id',Session('user.id'))->get();
	  
	                   $count = ballots::where('user_id',Session('user.id'))->count();
	                 $i='';
	                 $ballot = array();

                    for($i=0;$i<$count;$i++)
	                {
		               $ballot[] = ballot_peoples::select('name' )->where('ballot_id',$Ballot_no[$i]->id)->get();   
	                  }
					
					break;
					
				case 2:
				
					$registerpeople = PeopleModel::orderBy('people_id')->where('registered',1 )->where('user_id',Session('user.id'))->skip($limit1)->take($limit2)->get();
					
					$count = PeopleModel::orderBy('people_id')->where('registered',1 )->where('user_id',Session('user.id'))->count();
					
					break;
				case 3:
					
					$registerpeople = PeopleModel::orderBy('people_id')->where('registered',0)->where('user_id',Session('user.id'))->skip($limit1)->take($limit2)->get();
					$count = PeopleModel::orderBy('people_id')->where('registered',0 )->where('user_id',Session('user.id'))->count();
					break;
				case 4:
				
					$registerpeople = PeopleModel::orderBy('people_id')->where('can_vote',1)->where('user_id',Session('user.id'))->skip($limit1)->take($limit2)->get();
					$count = PeopleModel::orderBy('people_id')->where('can_vote',1 )->where('user_id',Session('user.id'))->count();
					break;
			}
        $jsonArray = array(
			'status' => true,
			'count'  => $count,
			'countInc' => $request->count,
		    'Ballot_no' =>$Ballot_no,
			'dataArray' =>$ballot,
			'flag' 		 => 0);
			
			echo json_encode($jsonArray); exit();
		 }
  public function deleteballot(Request $request)
	{
		
		$teller_id = $request->teller_id ;
		//print_r($teller_id);
		$ballotdelete=DB::table('ballots')->where('id', '=', $teller_id)->delete();
		$ballotdelete=DB::table('ballot_peoples')->where('ballot_id', '=', $teller_id)->delete();	
		if($ballotdelete > 0){
			$json_array = array('status'=> true);
			
		}else{
			$json_array = array('status'=> false);
		}
		
		echo json_encode($json_array);exit();
		
	}
	 public function editballot(Request $request)
	 {
		 $ballot_id = $request->ballot_id ;
		 
		$editdata = ballots::select('id','ballot_no' )->where('id',$ballot_id)->get();
		$ballotname = ballot_peoples::select('name' )->where('ballot_id',$ballot_id)->get();
		
		 $jsonArray = array(
			'status' => true,
			'editdata' =>$editdata[0]->id,
			'ballotname' =>$ballotname,
			'ballot_no'=>$editdata[0]->ballot_no,
			'flag' 		 => 0);
		echo json_encode($jsonArray); exit();
	 }

   public function editinsertballot(Request $request)
	 {   
		 
		  $ballotdelete=DB::table('ballot_peoples')->where('ballot_id', '=', $request['id'])->delete();
			$date  = date('Y-m-d H:i:s');
			
			 $data = array(
                  array('ballot_id'=>$request['id'], 'name'=> $request['ballot1'],'updated_on'=> $date),
                  array('ballot_id'=>$request['id'], 'name'=> $request['ballot2'],'updated_on'=> $date),
				  array('ballot_id'=>$request['id'], 'name'=> $request['ballot3'],'updated_on'=> $date),
				  array('ballot_id'=>$request['id'], 'name'=> $request['ballot4'],'updated_on'=> $date),
				  array('ballot_id'=>$request['id'], 'name'=> $request['ballot5'],'updated_on'=> $date),
				  array('ballot_id'=>$request['id'], 'name'=> $request['ballot6'],'updated_on'=> $date),
				  array('ballot_id'=>$request['id'], 'name'=> $request['ballot7'],'updated_on'=> $date),
				  array('ballot_id'=>$request['id'], 'name'=> $request['ballot8'],'updated_on'=> $date),
				  array('ballot_id'=>$request['id'], 'name'=> $request['ballot9'],'updated_on'=> $date),
				 );
				 
		DB::table('ballot_peoples')->insert($data);
		$count = Ballots::orderBy('id')->where('id',$request['id'])->count();
		$Ballot_no = ballots::select('id','ballot_no' )->where('id',$request['id'])->get();
		
		$ballotname = ballot_peoples::select('name' )->where('ballot_id',$request['id'])->get();
		 
		 $jsonArray = array(
		        'Ballot_no' =>$Ballot_no[0]->id,
			    'ballotname' =>$ballotname,
			    'ballot_no'=>$Ballot_no[0]->ballot_no,
				'status' => true,
				'count' => $count,
				'flag' 		 => 0);
				echo json_encode($jsonArray); exit();
	 }
	 
	  public function result(Request $request){
	   $teller = DB::table('users')->where('parents_id',Session('user.id'))->get();
	   
	   $jsonArray = array(
		'uri'  		 => 'results',
		'teller'     =>   $teller,
		'flag' 		 => 0);
		
		//echo'<pre>';print_r($jsonArray->Ballot_no);die;
		
		return view('result')->with($jsonArray);
		
	   
	}
	 public function finalresult(Request $request){
	    $people = DB::table('people')->where('user_id',Session('user.id'))->count();
		$registerpeople = DB::table('people')->where('user_id',Session('user.id'))->where('registered',1)->count();
		$nonregisterpeople = DB::table('people')->where('user_id',Session('user.id'))->where('registered',0)->count();
		$totalballot = DB::table('ballots')
            ->join('ballot_peoples', 'ballots.id', '=', 'ballot_peoples.ballot_id')
            ->select('ballot_peoples.*', 'ballots.ballot_no')
            ->count();
		$statusid = ballots::select('id')->where('user_id',Session('user.id'))->where('status',1)->get();
		$validballot = DB::table('ballot_peoples')->whereIn('ballot_id',$statusid)->count();
		 
		 $statusid = ballots::select('id')->where('user_id',Session('user.id'))->where('status',0)->get();
		 $unvalidballot = DB::table('ballot_peoples')->whereIn('ballot_id',$statusid)->count();
	   $jsonArray = array(
		'uri'  		       => 'results',
		'people'           => $people,
		'registerpeople'   => $registerpeople,
		'nonregisterpeople'=> $nonregisterpeople,
		'totalballot'      => $totalballot,
		'validballot'      => $validballot,
		'unvalidballot'      => $unvalidballot,
		'flag' 		       => 0);
		
		//echo'<pre>';print_r($jsonArray->Ballot_no);die;
		
		return view('finalresult')->with($jsonArray);
		
	   
	}
	
	public function ballotsdone(Request $request){
		$res = DB::table('ballots')
            ->where('user_id', Session('user.id'))
            ->update(['status'=>1]);
	  $jsonArray = array(
		'uri'  		 => 'ballots',
		'res'        =>  $res,
		'flag' 		 => 0);
	echo json_encode($jsonArray); exit();
    }
	public function tellerballot(Request $request){
		print_r('aaaa');die;
	}
	
}
