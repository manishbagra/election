<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class StateModel extends Model
{
    protected $table = 'states';
	
	protected $primaryKey = 'id';
	
	protected $fillable = ['name', 'country_id'];
}
