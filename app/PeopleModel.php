<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PeopleModel extends Model
{
    protected $table = 'people';
	
	protected $primaryKey = 'people_id';
	
	protected $fillable = ['user_id', 'surename', 'lastname', 'baha_id', 'community', 'registered', 'voting_type', 'can_vote', 'created_on', 'updated_on'];
}
