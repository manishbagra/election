@extends('layouts.app')

@section('title')
	step-5
@stop

@section('content')

<section class="midle-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="card-box drop-file">
          <h3><img src="images/logo-icon.png" alt="" title="" class="img-fluid"><span>Election</span> Service</h3>
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">And in the end, upload your exel file!</h4>
				    <form method="post" enctype="multipart/form-data" action="javascript:void(0)" id="doc_upload">
						<div class="form-group files color">
							<input type="file" name="doc_file" id="doc_file" class="form-control" multiple="">
							<label id="file_name">Drop Your File Here!
							<span>or click to browse</span></label>
						</div>
				 
						<div class="form-group row">
						  <div class="col-sm-12">
							<button class="btn btn-color-btn btn-block" id="upload_excel_csv"> Next</button>
						  </div>
						</div>
					</form>
            </div>
          </div>
        </div>
		
		<div class="card-box upload-file">
          <h3><img src="images/logo-icon.png" alt="" title="" class="img-fluid"><span>Election</span> Service</h3>
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">And in the end, upload your exel file!</h4>
              <form method="post" enctype="multipart/form-data" action="javascript:void(0)" id="#">
				<div class="form-group files color">
               
					<h5 class="file-name">Uploading <em id='FName'>  NSA_California_U1.xls </h5>
					<div class="form-group row">
					 
					  <div class="col-sm-10">
						<span class="line"></span>
					  </div>
					   <label for=" " class="col-sm-1 col-form-label"> <i class="material-icons">cancel</i> </label>
						<div class="col-sm-1">
					   
					  </div>
					</div>
					 <h6 class="upload-pres">%1</h6>
               
				</div>
              
            
         
                <div class="form-group row">
                  <div class="col-sm-12">
                    <button class="btn btn-color-btn btn-block" id="storeInTable"> Next</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
		
		
		<div class="card-box complet-file">
          <h3><img src="images/logo-icon.png" alt="" title="" class="img-fluid"><span>Election</span> Service</h3>
          <div class="card">
            <div class="card-body">  <h4 class="card-title"> Your file has been uploded successfully!</h4>
              <form method="post" action="javascript:void(0)" id="#">
               <div class="form-group files color">
                <div class="row">
                 <div class="col-sm-3">
                   <img src="images/icon-upload-col.png" alt="" title="" class="img-fluid"/> 
                  </div>
                  <div class="col-sm-7">
                    <p class="file-done"><em id="fl_nm">NSA_California_U1.xls</em></p>
                  </div>
                   <label for=" " class="col-sm-1 col-form-label"> <i class="material-icons">cancel</i> </label>
                    <div class="col-sm-1">
                   
                  </div>
                </div>
                
               
              </div>
              
            
         
                <div class="form-group row">
                  <div class="col-sm-12">
                    <button class="btn btn-color-btn btn-block" id="finalStoreData"> Next</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        
      <!-- //  card box section close  now //-->
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
</section>
@endsection