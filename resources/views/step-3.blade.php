@extends('layouts.app')

@section('title')
	Step -3
@stop

@section('content')

<section class="midle-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="card-box check-box">
          <h3><img src="/images/logo-icon.png" alt="" title="" class="img-fluid"><span>Election</span> Service</h3>
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Please Select the Election Type</h4>
              <form method="POST" action="{{url('/store_election_type')}}" >
			  {!!csrf_field()!!}
			  <input type="hidden" name="user_id" id="user_id">
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label>
						<input type="radio" name="radio" id="LSA" value="1" <?php if(Session('user.e_type')==1){ echo 'checked';  } ?>  /> <span class="label-text">LSA</span>
					</label>
                  </div>
                  <div class="col-sm-6">
                  <label>
						<input type="radio"  name="radio" id="NSA" value="2" <?php if(Session('user.e_type')==2){ echo 'checked';  } ?> /> <span class="label-text">NSA</span>
					</label>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6">
                   <label>
						<input type="radio"  name="radio" id="reg_coun" value="3" <?php if(Session('user.e_type')==3){ echo 'checked';  } ?> /> <span class="label-text">Regional Council</span>
					</label>
                  </div>
                  <div class="col-sm-6">
                  <label>
						<input type="radio"  name="radio" id="unit_conv" value="4" <?php if(Session('user.e_type')==4){ echo 'checked';  } ?> /><span class="label-text">Unit Convention</span>
					</label>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-12">
				  
                   
					
					@if($flag==1)
					<a href="{{url('/people')}}" class="btn btn-color-btn btn-block" style="color:white">Go to Dashboard</a>
                    @else
						 <button class="btn btn-color-btn btn-block"> Next</button></br>
					@endif
					
				  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        
        <!-- //  card box section close  now //--> 
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
</section>
@endsection