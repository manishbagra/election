 <div class="col-md-3">
      <div class="nav-left">
        <ul>
          <li>
            <div class="logo">
				<a href="#"> 
					<img src="images/logo-icon-color.png" alt="election-logo" title="Election service logo" class="img-fluid"/>
					<span> <strong> Election</strong> Service </span>
				</a>
			</div>
          </li>
          
        @if($flag == 0)
			<li> <div class="profil"> <i class="material-icons">account_circle</i>
				<h4>XYZ <lable>{{Session('user.email')}}</lable> <span class="badge badge-secondary">chief teller </span></h4></div>
			</li> 
			<li><div class="nav-sec <?php if($uri == 'ballots'){ echo 'active'; } ?>"><a href="{{url('/ballots')}}"><i class="material-icons">layers</i>Ballots </a> </div></li>
			<li><div class="nav-sec <?php if($uri == 'tellers'){ echo 'active'; } ?>"> <a href="{{url('/tellers')}}"><i class="material-icons">person_add</i> Tellers </a> </div></li>
			<li><div class="nav-sec <?php if($uri == 'people'){ echo 'active'; } ?>"> <a href="{{url('/people')}}"><i class="material-icons">group</i> People </a> </div></li>
			<li><div class="nav-sec <?php if($uri == 'results'){ echo 'active'; } ?>"> <a href="{{url('/results')}}"><i class="material-icons">view_list</i> Results </a> </div></li>
		@endif
		
		@if($flag == 1)
			
			<li><div class="profil"> <i class="material-icons">account_circle</i>
				<h4>XYZ <lable>{{ $teller->email }}</lable> <span class="badge badge-secondary teller"> teller </span></h4></div>
			</li> 
			<li><div class="nav-sec <?php if($uri == 'ballots'){ echo 'active'; } ?>"><a href="{{ url('/teller_panel/ballots')}}"><i class="material-icons">layers</i>Ballots </a> </div></li>
			<li> <div class="nav-sec active"> <a href="{{ url('/teller_panel')}}/{{ base64_encode($teller->unique_id) }}"> <i class="material-icons">group</i> People </a> </div></li>
		    
		@endif
		   
        </ul>
      </div>
    </div>
	