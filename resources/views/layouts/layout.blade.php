<!DOCTYPE html>
<html lang="en" >
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content='{{ csrf_token() }}' />
<title>@yield('title')</title>
<!-- Bootstrap CSS -->
 {!!Html::style('css/bootstrap.min.css')!!}
 {!!Html::style('css/make-custome.css')!!}

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
</head>

<body class="bg-full" >
<div class="container-fluid admin">
  <div class="row">
   
       
     
        <!-- Left Menu Navigation -->
        @include('layouts.header')
        <!-- /Left Menu Navigation -->
        
        <!-- Top Right Side Navigation -->
        @include('layouts.sidebar')
        <!-- /Top Right Side Navigation -->
        
        <!-- page content -->
        @yield('content')
        <!-- /page content -->
		
  
	<!-- jQuery -->
	

	{!!Html::script('js/app.js')!!}
    {!!Html::script('js/jquery3.2.1.min.js')!!}
	{!!Html::script('js/popper.min.js')!!}
	{!!Html::script('js/bootstrap.min.js')!!}
	
	<script src="https://use.fontawesome.com/abf49d706d.js"></script>
	<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
    {!!Html::script('js/election_service.js')!!}
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	
	 </div>
</div>
  
   
	
</body>
</html>