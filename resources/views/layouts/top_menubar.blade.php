<div class="dg_main_navigation clearfix">
    	<div class="dg_author_name"><h5>Welcome, {!!Helper::getAdminUserName()!!}</h5></div>
        <ul class="pull-right dg_second_menu">
            <li>
                <a href="index-2.html">
                    <i class="flaticon-home60"></i>
                    <span>Home</span>
	            </a>
            </li>
            <li>
                <a href="sales_report.html">
                    <i class="flaticon-coin11"></i>
                    <span>Sales</span>
                </a>
            </li>
            <li>
                <a href="administrator.html">
                    <i class="flaticon-profile7"></i>
                    <span>Users</span>
              	</a>
            </li>
            <li>
                <a href="item_management.html">
                    <i class="flaticon-list91"></i>
                    <span>Products</span>
                </a>
            </li>
            <li>
            <a href="element.html">
                <i class="flaticon-circular8"></i>
                <span>Pages</span>
			</a>
            </li>
            <li>
            <a href="template_settings.html">
                <i class="flaticon-pencil108"></i>
                <span>Settings</span>
			</a>
            </li>
        </ul>
    </div>