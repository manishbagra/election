<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content='{{ csrf_token() }}' />
{{ csrf_field() }}
 <title>@yield('title')</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/make-custome.css') }}">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{ asset('js/election_service.js') }}"></script>
</head>


<body>
<header></header>
@yield('content')
<script src="{{asset('js/jquery-3.2.1.slim.min.js')}}"></script> 
<script src="{{asset('js/popper.min.js')}}"></script> 
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
<script src="https://use.fontawesome.com/abf49d706d.js"></script>
</body>
</html>
