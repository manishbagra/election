@extends('layouts.layout')

@section('title')
	Ballots
@stop
@section('content')

<div class="col-md-9">
    <div class="main-admin">
      <div class="head">
        <h4 class="title-pop">Ballots
          <div class="ser"><i class="material-icons">search</i></div>
        </h4>
      </div>
      <!--// header section  close now //-->
     
      <div class="filer-tool">
        <ul class="list-inline btn-filt">
          <li class="list-inline-item"><a data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"> <i class="material-icons">filter_list</i> FILTERS</a></li>
          <li class="list-inline-item"><a href="#"> <i class="material-icons">sort</i> SORT</a></li>
          <li class="list-inline-item dang"><a href="#" data-toggle="modal" data-target=".bd-example-modal-lg"> ADD BALLOT</a></li>
		  <li class="list-inline-item dang" id="done"><a href="javascript:void(0)">DONE</a></li>
		   <span id="updPeoSec">Insert Successfully....!</span>
        </ul>
        <ul class="list-inline upload-tile-filt">
          <li class="list-inline-item bold" id='ballotCount'><a href="#">{{$count}} ADDED BALLOTS</a></li>
        </ul>
      </div>
      <!-- // filter and tool section close  now //-->
      <div>
        <div>
          <div  class="collapse multi-collapse file-open" id="multiCollapseExample1">
            <ul id="listballot">
              <li> <a href="#" >
                <label>
                  <input type="radio" name="radio" id="allballot" value="1" checked="">
                  <span class="label-text"> All added ballots </span> </label>
                <strong>{{$count}}</strong></a> </li>
              <li class="title-non">
                <h4>Filter by registration <span></span></h4>
              </li>
              <li> <a href="#">
                <label>
                  <input type="radio" name="radio" id="chief" value="2" checked="">
                  <span class="label-text">Michel Saberi (You) <br/>
                 <strong class="color-1"> Countong ballots...</strong>
                  </span> </label>
                <strong>74</strong> </a> </li>
              <li class="filter-hr"><a href="#">
                <label>
                  <input type="radio" name="radio" id="teller" value="3" checked="">
                  <span class="label-text">Sara Rise<br/>
             <strong class="color-2">     Done  <i class="material-icons">check_circle</i></strong>
                  </span> </label>
                <strong>141</strong> </a></li>
              <li class="title-non">
                <h4>Filter by voting type <span></span></h4>
              </li>
              <li><a href="#">
                <label>
                  <input type="radio" name="radio" id="Accepted" value="4" checked="">
                  <span class="label-text">Accepted </span> </label>
                <strong>213</strong> </a> </li>
              <li><a href="#">
                <label>
                  <input type="radio" name="radio" id="Voided" value="5" checked="">
                  <span class="label-text">Voided </span> </label>
                <strong>2</strong> </a></li>
              
               
            </ul>
          </div>
        </div>
        <div id="ballot_section">
          <div class="mult-card">
            <div class="row">
			
			
			
             
				@if($Ballot_no->isEmpty())
					<div  class="col-md-6" style="margin-left:160px;text-align:center;padding-left:166px; " >
                    <div class="card">
					  <div class="card-body" style="background: #ee496d;color: #fff; "><strong style="font-size:18px;">No recode Found</strong> </div>
					  </div>
					  </div>
				@else
				
				
				@foreach($Ballot_no as $Ballot)
				
				<div class="col-md-4">
                <div class="card">
				<div class="card-header"><strong>{{$Ballot->ballot_no}}</strong> 
				
				 <i style="margin-left:160px;"class="material-icons edit_delete" id="{{$Ballot->id}}">delete</i>
				<i style=""class="material-icons edit_delete1"id="{{$Ballot->id}}"><a data-toggle="modal" data-target=".bd-example-modal-lg1">edit</a></i></div>
				<div class="card-body">
				 
				 @foreach ($ballot[0] as $key => $ballots)
				  <ul>
                      <li>{{$ballots->name}}</li>
                   </ul> 
					 @endforeach 
					 
                  </div>
				  </div>
              </div>
                 @endforeach
			  	@endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--// model popup designe //-->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div id="editballots"class="row" >
        <div class="col-md-8 right-section">
		<form  method="Post" action="javascript:void(0)">
		  <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="modal-header">
            <div class=" row head-widt">
              <div class="col-md-7">
                <h5 class="modal-title" id="exampleModalLongTitle">ADD BALLOT</h5>
              </div>
              <div class="col-md-5 text-right head-widt">
                <div class="form-group row">
                  <label for="1" class="col-sm-8 col-form-label">Ballot number</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control vikashdada" id="ballot_no" name="ballot_no" placeholder="21">
                  </div>
                </div>
              </div>
              <div class="col-md-12 img-text"><img src="images/icon-light.jpg" class="img-fluid float-left" alt="" title=""> start typing to search in names. click on them to add them separately
                to the ballot <span>(or click enter to add the highlighted    search result)</span> </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
			   
                <ul class="add-input">
				
                  <li>
                    <div class="form-group row">
                      <label for="1" class="col-sm-1 col-form-label">1</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control ballotpeople" id="ballot1" name="ballot1" placeholder=" ">
                         
						<i class="material-icons">visibility_off</i> </div>
                    </div>
                  </li>
                  <li>
                    <div class="form-group row">
                      <label for="1" class="col-sm-1 col-form-label">2</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control ballotpeople" id="ballot2" name="ballot2" placeholder=" ">
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="form-group row">
                      <label for="1" class="col-sm-1 col-form-label">3</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control ballotpeople" id="ballot3" name="ballot3" placeholder=" ">
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="form-group row">
                      <label for="1" class="col-sm-1 col-form-label">4</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control ballotpeople" id="ballot4" name="ballot4" placeholder=" ">
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="form-group row">
                      <label for="1" class="col-sm-1 col-form-label">5</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control ballotpeople" id="ballot5" name="ballot5" placeholder=" ">
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="col-md-6">
                <ul class="add-input">
                  <li>
                    <div class="form-group row">
                      <label for="1" class="col-sm-1 col-form-label">6</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control ballotpeople" id="ballot6" name="ballot6" placeholder=" ">
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="form-group row">
                      <label for="1" class="col-sm-1 col-form-label">7</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control ballotpeople" id="ballot7" name="ballot7" placeholder=" ">
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="form-group row">
                      <label for="1" class="col-sm-1 col-form-label">8</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control ballotpeople" id="ballot8" name="ballot8" placeholder=" ">
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="form-group row">
                      <label for="1" class="col-sm-1 col-form-label">9</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control ballotpeople" id="ballot9" name="ballot9" placeholder=" ">
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="modal-footer chang"> 
			  <a href="javascript:void(0)" class="dismis" data-dismiss="modal">CANCEL</a> 
			  <a href="javascript:void(0)" id="add_ballot" class="btn btn-color-btn-n">DONE</a> 
		  </div>
		  </form>
        </div>
        <div class="col-md-4 left-section" id="bal"> 
          <!--   <ul>
          <li><a href="#">Abigail S.Katherine  <i class="material-icons">subdirectory_arrow_left</i></a></li>
          <li><a href="#">Abigail Nicola <i class="material-icons">subdirectory_arrow_left</i> </a></li>
          <li><a href="#">Abigail R. Natalie <i class="material-icons">subdirectory_arrow_left</i> </a></li>
          <li><a href="#">Abigaitione M. <i class="material-icons">subdirectory_arrow_left</i> </a></li>
          <li><a href="#">Abigail S.Katherine <i class="material-icons">subdirectory_arrow_left</i></a></li>
          <li><a href="#">Abigail Nicola <i class="material-icons">subdirectory_arrow_left</i> </a></li>
          <li><a href="#">Lillian R. Abigail</a></li>
          <li><a href="#">Ann M. Abigail</a></li>
                  </ul>-->
          
          <div class="card"  >
            <div class="card-body" > <img src="images/not-fount-icon.jpg" class="img-fluid" alt="" title="">
              <h4 class="card-title">Person You entered not found in
                the list!</h4>
              <a href="#" class="sumbit-away">Submit anyway</a>
              <p>Click on <i class="material-icons">visibility_off</i> icon if the name is
                not readable</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--// model popup designe for edit //-->
<div class="modal fade bd-example-modal-lg1" id="edit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div id="editballots"class="row" >
        <div class="col-md-8 right-section">
		<form  method="Post" action="javascript:void(0)">
		  <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="modal-header">
            <div class=" row head-widt">
              <div class="col-md-7">
                <h5 class="modal-title" id="exampleModalLongTitle">EDIT BALLOT</h5>
              </div>
              <div class="col-md-5 text-right head-widt">
                <div class="form-group row">
                  <label for="1" class="col-sm-8 col-form-label">Ballot number</label>
                  <div class="col-sm-4">
				      
                     <input type="text" class="form-control vikashdada" id="ballot_noedit" name="ballot_no" placeholder="21" >
                      <input type="hidden" id="editid">
				 </div>
                </div>
              </div>
              <div class="col-md-12 img-text"><img src="images/icon-light.jpg" class="img-fluid float-left" alt="" title=""> start typing to search in names. click on them to add them separately
                to the ballot <span>(or click enter to add the highlighted     search result)</span> </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="row editballot">
			  
             
			  
            </div>
          </div>
          <div class="modal-footer chang"> 
			  <a href="javascript:void(0)" class="dismis" data-dismiss="modal">CANCEL</a> 
			  <a href="javascript:void(0)" id="edit_ballot" class="btn btn-color-btn-n">update</a> 
		  </div>
		  </form>
        </div>
        <div class="col-md-4 left-section" id="autocompleteedit"> 
          <!--   <ul>
          <li><a href="#">Abigail S.Katherine  <i class="material-icons">subdirectory_arrow_left</i></a></li>
          <li><a href="#">Abigail Nicola <i class="material-icons">subdirectory_arrow_left</i> </a></li>
          <li><a href="#">Abigail R. Natalie <i class="material-icons">subdirectory_arrow_left</i> </a></li>
          <li><a href="#">Abigaitione M. <i class="material-icons">subdirectory_arrow_left</i> </a></li>
          <li><a href="#">Abigail S.Katherine <i class="material-icons">subdirectory_arrow_left</i></a></li>
          <li><a href="#">Abigail Nicola <i class="material-icons">subdirectory_arrow_left</i> </a></li>
          <li><a href="#">Lillian R. Abigail</a></li>
          <li><a href="#">Ann M. Abigail</a></li>
                  </ul>-->
          
          <div class="card"  >
            <div class="card-body" > <img src="images/not-fount-icon.jpg" class="img-fluid" alt="" title="">
              <h4 class="card-title">Person You entered not found in
                the list!</h4>
              <a href="#" class="sumbit-away">Submit anyway</a>
              <p>Click on <i class="material-icons">visibility_off</i> icon if the name is
                not readable</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection