@extends('layouts.app')

@section('title')
	OTP Comfrim
@stop

@section('content')

<?php
if(Session("data.Time") == ""){
	$Time = "";
}else{
	$Time = Session("data.Time");
}
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<section class="midle-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="card-box">
          <h3><img src="images/logo-icon.png" alt="" title="" class="img-fluid"><span>Election</span> Service</h3>
          <div class="card  setp-b">
            <div class="card-body">
            <h4 class="card-title">Chief Teller Sign up</h4><span>Please enter this otp : {{Session::get('data')['otp']}}</span>
            <p class="red-title">  <strong>We've sent the code</strong> to your phone number & email address. Please enter the code below.</p>
			<ul>
				<li>{{Session::get('data')['code']}}  {{Session::get('data')['phone']}}</li>
				<li>{{Session::get('data')['email']}}</li>
				<li><a href="{{ url('/') }}"> Edit phone number & email address </a></li>
			</ul>

				<form class="alig" method="post" action="javascript:void(0)"data-toggle="validator">
				{!!csrf_field()!!}

					<div class="form-group row">
					  <label for=" " class="col-sm-1 col-form-label"> <i class="material-icons">vpn_key</i></label>
					  <div class="col-sm-11">
						<input type="text" class="form-control" maxlength="4" id="otp_confirm" name="otp_confirm" placeholder="Enter your code here">
						<input type="hidden"  id="otp_val" name="otp_val" value="{{Session::get('data')['otp']}}"> 
						
						<input type='hidden' name='Time' id='Time' value="{{$Time}}" >
						
						<i class="material-icons" id="success-otp">&#xE834;</i>
						<i class="material-icons" id="error-otp">&#xE909;</i>
					  </div>
					  <span class="err_css" id="otp_exp">OTP expired please re-send.</span>
					  
					</div>
                 
				</form>
			<p class="time-pin">You will be able to request a new code in <span id="count"></span> </p>

            </div>
          </div>
        </div>
        
      <!-- //  card box section close  now //-->
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
</section>
@endsection