@extends('layouts.app')

@section('title')
	Election Service
@stop

@section('content')

<?php
if(Session("data.email") == ""){
	$email = "";
}else{
	$email = Session("data.email");
}
if(Session("data.phone") == ""){
	$phone = "";
}else{
	$phone = Session("data.phone");
}

?>

<section class="midle-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="card-box">
          <h3><img src="images/logo-icon.png" alt="Service" title="Service" class="img-fluid"><span>Election</span> Service</h3>
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Chief Teller Sign up</h4>
			  
			 
			   
             <form class="alig" method="POST" action="{{url('/second_step_reg')}}">
			@if(count($errors))
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.
						<br/>
						<ul>
							@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group row {{ $errors->has('email') ? 'has-error' : '' }}">
                  <label for="email " class="col-sm-1 col-form-label"> <i class="material-icons">mail_outline</i></label>
                  <div class="col-sm-11">
                    <input type="email" class="form-control" required id="email" name="email" value="{{$email}}" placeholder="Email Address">
				     <span class="text-danger">{{ $errors->first('email') }}</span>
				  </div>
                </div>
                <div class="form-group row">
                  <label for="phone" class="col-sm-1 col-form-label"> <i class="material-icons">phone</i> </label>
                  <div class="col-sm-2"> <small id="code" class="form-text text-muted">Code</small>
                    <input type="text" readonly  class="form-control" value="+61" id="code " name="code" >
				  </div>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" required id="phone" name="phone" value="{{$phone}}"  placeholder="Phone Number"> 
				     <span class="text-danger">{{ $errors->first('phone') }}</span>
				  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-12">
                    <button class="btn btn-color-btn btn-block"> Next</button>
					<span id="countdownTimer"></span>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        
      <!-- //  card box section close  now //-->
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
</section>
@endsection

