@extends('layouts.app')

@section('title')
	step-7
@stop

@section('content')

<section class="midle-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="card-box complet-file">
          <h3><img src="images/logo-icon.png" alt="" title="" class="img-fluid"><span>Election</span> Service</h3>
          <div class="card">
            <div class="card-body">  <h4 class="card-title"> Your file has been uploded successfully!</h4>
              <form method="post" action="#" id="#">
               <div class="form-group files color">
                <div class="row">
                 <div class="col-sm-3">
                   <img src="images/icon-upload-col.png" alt="" title="" class="img-fluid"/> 
                  </div>
                  <div class="col-sm-7">
                    <p class="file-done"><em>NSA_California_U1.xls</em></p>
                  </div>
                   <label for=" " class="col-sm-1 col-form-label"> <i class="material-icons">cancel</i> </label>
                    <div class="col-sm-1">
                   
                  </div>
                </div>
                
               
              </div>
              
            
         
                <div class="form-group row">
                  <div class="col-sm-12">
                    <button class="btn btn-color-btn btn-block"> Next</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        
      <!-- //  card box section close  now //-->
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
</section>

@endsection