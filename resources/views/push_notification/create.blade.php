@extends('layouts.layout')

@section('title')
	New Message
@stop

@section('content')
<section id="dg-main-content">
	<div class="dg_main_navigation clearfix">
        <h4><i class="fa fa-bell-o marginTB5 marginRight10"></i>Push Notifications</h4>
    </div>
    
    <section class="dg-wrapper">
        <div class="dg_bottom_content_section">
            <div class="row">
            	<div class="col-md-12">
                    <div class="dg_heading">
                        <h5>New Message</h5>
                    </div>
                    @if (count($errors))
                    <ul class="errorFormMessage">
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    @endif
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @elseif ($message = Session::get('danger'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <div class="dg_main_form spacer20">
                        <form action="{!!url('/push-notification')!!}" method="post" data-toggle="validator">
                            {!!csrf_field()!!}
							
							<div class="form-group dg_single_form form_required">
                                <label>Driver</label>
                                <span>
                                	<select class="form-control" id='pn_dr_id' name='pn_dr_id'>
									   <option value='all'>All</option>
									   <?php foreach($Results as $result){ ?>
									   <option value="<?php echo $result['id'] ?>"><?php echo $result['fname']."&nbsp;&nbsp;".$result['fname'].'&nbsp;&nbsp;( <span style="color:green;">'.$result['email'].' </span> )'; ?></option>
									   <?php } ?>
									</select>
                                	<span class="help-block with-errors"></span>
                                </span>
                            </div>
							
                            <div class="form-group dg_single_form form_required">
                                <label>Title</label>
                                <span>
                                	<input type="text" name="pn_title" id="pn_title" value="{!!old('pn_title')!!}" class="form-control" placeholder="Enter Title" required>
                                	<span class="help-block with-errors"></span>
                                </span>
                            </div>
                            <div class="form-group dg_single_form form_required">
                            	<label>Message Text</label>
                            	<span>
                            		<textarea name="pn_message" id="pn_message" value="{!!old('pn_message')!!}" class="form-control" placeholder="Enter Message" required></textarea>
                            		<span class="help-block with-errors"></span>
                            	</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="pull-left" style="margin-left:15%">
                                    <input type="submit" name="submitbtn" id="submitButton" value="Send" class="btn_black dg_big_btn"/>
                                    <input type="reset" name="clearbtn" id="clearButton" value="Clear" class="btn_red dg_big_btn"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection