@extends('layouts.layout')

@section('title')
	Push Notifications
@stop

@section('content')
<section id="dg-main-content">
	<div class="dg_main_navigation clearfix">
        <h4><i class="fa fa-bell-o marginTB5 marginRight10"></i>Push Notifications</h4>
    </div>
    
    <section class="dg-wrapper">
        <div class="dg_bottom_content_section">
            <div class="row">
                <div class="col-md-12">
                    <div class="dg_heading">
                        <h5>Push Notification Lists</h5>
                        <span class="pull-right"><a href="{{url('/push-notification/create')}}"><span class="btn_purple dg_btn">New Message</span></a></span>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @elseif ($message = Session::get('danger'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <table class="display dg_main_table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Time</th>
                                <th>Title</th>
                                <th>Message</th>
                                <th>Status </th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($PushNotifyRst as $PushNotifyVal)
                            <tr>
                                <td>{{++$PSJ}}</td>
                                <td>{!!$PushNotifyVal->PN_Date!!}</td>
                                <td>{!!$PushNotifyVal->PN_Title!!}</td>
                                <td>{!!$PushNotifyVal->PN_Message!!}</td>
                                <td>{!!($PushNotifyVal->PN_Status == 1)?"Success":(($PushNotifyVal->PN_Status == 2)?"Error":"Error");!!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $PushNotifyRst->render() !!}
                </div>
            </div>
        </div>
    </section>
</section>
<script>
	function ConfirmAction(msg)
	{
		var x = confirm(msg);
		if(x)
			return true;
		else
			return false;
	}
</script>
@endsection