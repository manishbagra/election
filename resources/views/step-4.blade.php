@extends('layouts.app')

@section('title')
	Step -4
@stop

@section('content')


<section class="midle-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="card-box">
          <h3><img src="images/logo-icon.png" alt="" title="" class="img-fluid"><span>Election</span> Service</h3>
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Please Select the Place of Election</h4>
              <form method="POST" action="{{url('/getLocationsDetails')}}" >
			  {!!csrf_field()!!}
                <div class="form-group row">
                  <label for=" " class="col-sm-1 col-form-label"> <i class="material-icons">location_on</i> </label>
                  <div class="col-sm-11">
                    <select disabled="true" class="form-control" id="country_id" name="country">
					  @foreach ($countries as $key => $country)
					    <option  value="{{@$country->id}}">{{@$country->name}}</option>
					  @endforeach
					</select>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6">
                    <select class="form-control" id="state_id" name="state">
					 <option value="">Select State</option>
					   @foreach ($all_state as $key => $state)
					    <option  value="{{@$state->id}}">{{@$state->name}}</option>
					  @endforeach
					</select>
                  </div>
                  <div class="col-sm-6">
                    <select required class="form-control" id="city_id" name="city">
					  <option value="">Select City</option>
					</select>
                  </div>
                  
                </div>
                <div class="form-group row">
                  <div class="col-sm-12">
                    <button class="btn btn-color-btn btn-block"> Next</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        
      <!-- //  card box section close  now //-->
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
</section>
@endsection