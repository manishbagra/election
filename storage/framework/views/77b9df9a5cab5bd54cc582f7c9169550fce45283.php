<?php $__env->startSection('title'); ?>
	People
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

 <div class="col-md-9">
      <div class="main-admin">
        <div class="head"> <h4 class="title-pop">PEOPLE  <div class="ser"><i class="material-icons">search</i></div></h4>
       
         </div>
        <!--// header section  close now //-->
         
        <div class="filer-tool">
	    <?php if($flag == 0): ?>
          <ul class="list-inline btn-filt">
            <li class="list-inline-item open" ><a data-toggle="collapse" id='filterBtn' href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"> <i class="material-icons">filter_list</i> FILTERS</a></li>
            <li class="list-inline-item dang" id="savePeopleDetails"><a href="javascript:void(0)">DONE & SAVE</a></li>
			<span id="updPeoSec"> Updated Successfully....!</span>
          </ul>
		  <?php endif; ?>
          <ul class="list-inline upload-tile-filt">
			<?php if($count>0): ?>
				<li class="list-inline-item"><a href="#"> <?php echo e(\Carbon\Carbon::parse($allPeople[0]->created_on)->format('Y-m-d H:i')); ?> </a></li>
	<li class="list-inline-item" id="file_name"><a href="#"><?php echo e($filename[0]->file_name); ?></a></li>
				<li class="list-inline-item bold"><a href="#"><?php echo e($count); ?> People </a></li>
			<?php endif; ?>	
          </ul>
        </div>
        <!-- // filter and tool section close  now //-->
		
        
       
        <div  class="collapse multi-collapse file-open" id="multiCollapseExample1">
        <ul id="listpeople">
        <li> <a href="#" > <label>
						<input type="radio" name="radio" id="allpeople" value="1" checked=""> <span class="label-text"> All People </span>
					</label>   <strong><?php echo e($count); ?></strong></a> </li>
        <li class="title-non">
        <h4>Filter by registration <span></span></h4>
        </li>
         <li> <a href="#"> <label>
						<input type="radio" name="radio" id="register" value="2"> <span class="label-text">Registered</span> 
					</label> <strong><?php echo e($registerpeople); ?></strong> </a> </li>
         <li class="filter-hr"><a href="#"> <label>
						<input type="radio" name="radio" id="NotRegistered"  value="3"> <span class="label-text">Not Registered </span> 
					</label> <strong><?php echo e($notregisterpeople); ?></strong> </a></li>
         <li class="title-non">
        <h4>Filter by voting type <span></span></h4>
        </li>
         <li><a href="#"> <label>
						<input type="radio" name="radio" id="VotinginPerson"  value="4"> <span class="label-text">Voting in Person </span>
					</label>  <strong><?php echo e($votintype); ?></strong> </a> </li>
         
         <li><a href="#"> <label>
						<input type="radio" name="radio" id="PostalBallot" value="6"> <span class="label-text">Postal Ballot </span>
					</label>  <strong>38</strong></a></li>
         <li><a href="#"> <label>
						<input type="radio" name="radio" id="AbsenteeBallot" value="7"> <span class="label-text">Absentee Ballot</span>
					</label> </a></li>
        </ul>
        </div>
       
        <div class="table list-data" id="peoplelist">
          <table class="table table-hover table-striped">
            <thead>
              <tr>
				<th scope="col">No.</th>
                <th scope="col">Surename</th>
                <th scope="col">Last Name</th>
                <th scope="col">Baha'i ID</th>
                <th scope="col">Community</th>
                <th scope="col">Registered</th>
                <th scope="col">Voting Type</th>
                <th scope="col">Can Vote</th>
              </tr>
            </thead>
            <tbody>
			<?php $__currentLoopData = $allPeople; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $people): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			
			<form class="peopleSave" id="peopleSave_<?php echo e($key); ?>">
			
              <tr>
			    <td><?php echo e(++$PSJ); ?></td>
                <td><?php echo e($people->surename); ?></td>
                <td><?php echo e($people->lastname); ?></td>
                <td><?php echo e($people->baha_id); ?></td>
                <td><?php echo e($people->community); ?></td>
                <td>
				<input type="hidden" name="id" value="<?php echo e($people->people_id); ?>" />
					<div class="material-switch pull-right">
						<input id="reg_<?php echo e($key); ?>" value="1" name="register" <?php if($people->registered == 1){ echo 'checked'; } ?> type="checkbox"/>
						<label for="reg_<?php echo e($key); ?>" class="label-success"></label>
					</div>
				</td>
                <td>
					<select class="form-control" name="voting" id="exampleFormControlSelect1">
						<option value="0"  <?php if($people->voting_type == 0){ echo 'selected'; } ?> >Voting in Person</option>
						<option value="2" <?php if($people->voting_type == 2){ echo 'selected'; } ?> >2</option>
						<option value="3" <?php if($people->voting_type == 3){ echo 'selected'; } ?> >3</option>
						<option value="4" <?php if($people->voting_type == 4){ echo 'selected'; } ?> >4</option>
						<option value="5" <?php if($people->voting_type == 5){ echo 'selected'; } ?> >5</option>
					</select>
				</td>
                <td>
					<div class="material-switch pull-right">
						<input id="vote_<?php echo e($key); ?>" value="1" name="can_vote" type="checkbox" <?php if($people->can_vote == 1){ echo 'checked'; } ?> />
						<label for="vote_<?php echo e($key); ?>" class="label-success"></label>
					</div>
				</td>
              </tr>
			 
			</form>
			
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
			 
            </tbody>
          </table>
	  
		 <?php echo $allPeople->render(); ?>

		 
        </div>
      </div>
    </div>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>