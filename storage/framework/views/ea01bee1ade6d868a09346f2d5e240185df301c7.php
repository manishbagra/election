<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content='<?php echo e(csrf_token()); ?>' />
<?php echo e(csrf_field()); ?>

 <title><?php echo $__env->yieldContent('title'); ?></title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/make-custome.css')); ?>">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo e(asset('js/election_service.js')); ?>"></script>
</head>


<body>
<header></header>
<?php echo $__env->yieldContent('content'); ?>
<script src="<?php echo e(asset('js/jquery-3.2.1.slim.min.js')); ?>"></script> 
<script src="<?php echo e(asset('js/popper.min.js')); ?>"></script> 
<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/app.js')); ?>"></script>
<script src="https://use.fontawesome.com/abf49d706d.js"></script>
</body>
</html>
