<?php $__env->startSection('title'); ?>
	Teller
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

 <div class="col-md-9">
      <div class="main-admin">
        <div class="head">
          <h4 class="title-pop">TELLERS
            <div class="ser"><i class="material-icons">search</i></div>
          </h4>
        </div>
        <!--// header section  close now //-->
		
        <div class="card-full">
          <div class="row">

            <div class="col-md-6">
              <div class="card">
                <div class="card-body">
                  <div class="media"> <i class="material-icons mr-3 alig-icon">account_circle</i>
                    <div class="media-body">
                      <h5 class="mt-0">XYZ (You)</h5>
                      <p>Countong ballots... </p>
                      <span>chief teller</span> </div>
                  </div>
                </div>
                <div class="card-body-a">
                  <ul>
                    <li> <i class="material-icons">phone</i>+<?php echo e(Session('user.code')); ?>  <?php echo e(Session('user.phone')); ?> </li>
                    <li> <i class="material-icons">mail_outline</i><?php echo e(Session('user.email')); ?></li>
                    <input type="hidden" name="chef_id" id="chef_id" value="<?php echo e(Session('user.id')); ?>" > 
				  </ul>
                </div>
                <span class="both-border"> Signed in as Chef Teller of The California LSA Election in <strong>17 March 2017 - 12:55 PM</strong> </span>
             
               <h4 class="footer"> <label> ADDED <span id='tellerCount'><?php echo e($count); ?></span> BALOTS <i class="material-icons">keyboard_arrow_right</i></label></h4>
              </div>
            </div>
            
           <!-- //  card view section close now //-->
		   <?php $__currentLoopData = $showtellers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $showteller): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		   
             <div class="col-md-6" id="teller_<?php echo e($showteller->id); ?>" >
			 
              <div class="card">
			  
                <div class="card-body cardBodyCss">
                  <div class="media"> <i class="material-icons mr-3 alig-icon">account_circle</i>
                    <div class="media-body">
                      <h5 class="mt-0">ABC</h5>
                      <p class="color">Done <i class="material-icons">check_circle</i> </p>
                      <span>teller</span> </div>
                  </div>
                </div>
				

                <div class="card-body-a">
                  <ul>
                    <li> <i class="material-icons">phone</i> +<?php echo e($showteller->code); ?> <?php echo e($showteller->phone); ?> </li>
                    <li> <i class="material-icons">mail_outline</i> <?php echo e($showteller->email); ?></li>
                  </ul>
                </div>
                <span class="both-border"><i class="material-icons">link</i> Shared link : <a href="javascript:void(0)"><span id="text_<?php echo e($key); ?>" ><?php echo e($baseurl.'/teller_panel/'.base64_encode($showteller->unique_id)); ?> </span></a> <i class="material-icons copyClip" onClick="copyContent('text_<?php echo e($key); ?>');" >content_copy</i>
				 <span id='signCpy_<?php echo e($key); ?>' class='signcpy' >Copied</span>
				</span>
               
               <h4 class="footer removeTeller" id="<?php echo e($showteller->id); ?>">REMOVE<!--<label> ADDED 68 BALOTS <i class="material-icons">keyboard_arrow_right</i></label>--></h4>
              </div>
            </div>
			
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
            <!-- //  card view section close now //-->
            
           
			 
			 <div class="col-md-6">
			  <button type="button" class="btn btn-color btn-lg btn-block" data-toggle="modal" data-target="#exampleModal">+ ADD NEW TELLER</button>
            </div>
          </div>
        </div>
		  
      </div>
    </div>
 

<!-- Modal -->
<div class="modal small-popup fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header border-none">
        <h5 class="modal-title" id="exampleModalLabel">ADD TELLER</h5>
         </div>
      <div class="modal-body">
      <form class="alig" method="POST" action="javascript:void(0)">
	  <?php if(count($errors)): ?>
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.
						<br/>
						<ul>
							<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<li><?php echo e($error); ?></li>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul>
					</div>
				<?php endif; ?>
	     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
		
                <div class="form-group row <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                  <label for="email" class="col-sm-1 col-form-label"> <i class="material-icons">mail_outline</i> </label>
                  <div class="col-sm-11">
                    <input required type="text" class="form-control" id="email" name="email"   placeholder="Email Address">
					 <span class="text-danger"><?php echo e($errors->first('email')); ?></span>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="phone" class="col-sm-1 col-form-label"> <i class="material-icons">phone</i> </label>
                  <div class="col-sm-2"> 
                    <input required type="text" readonly class="form-control"  id="code" value="+61" name="code" >
					<span class="text-danger"><?php echo e($errors->first('code')); ?></span>
                  </div>
                  <div class="col-sm-9">
                   <input required type="text" class="form-control"  id="phone" name="phone"  placeholder="Phone Number"> 
				      <span class="text-danger"><?php echo e($errors->first('phone')); ?></span>
                  </div>
                </div>
				<div class="modal-footer" >
					<a href="javascript:void(0)" class="dismis" data-dismiss="modal">CANCEL</a>
					<a href="javascript:void(0)" id="add_teller" class="btn btn-color-btn" >DONE</a>
			  </div>
              </form>
      </div>
      
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>