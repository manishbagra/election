<?php $__env->startSection('title'); ?>
	Results
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-9">
      <div class="main-admin">
        <div class="head"> <h4 class="title-pop">RESULTS  <div class="ser"><i class="material-icons">search</i></div></h4>
       
         </div>
        <!--// header section  close now //-->
        
        <div class="filer-tool">
          <ul class="list-inline btn-filt">
           
            <li class="list-inline-item dang" id="generateresult"><a href="<?php echo e(url('/finalresult')); ?>"> DONE COUNTING & GENERATE RESULTS </a></li>
          </ul>
          <ul class="list-inline upload-tile-filt">
           
            <li class="list-inline-item bold"><a href="#"> 1 OF 1 TELLERS DONE </a></li>
          </ul>
        </div>
        <!-- // filter and tool section close  now //-->
        <div class="reslut">
         <div class="row">
		 <?php $__currentLoopData = $teller; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $tellers): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <div class="col-md-6">
         <div class="card"> <div class="card-body"><div class="media">
    <i class="material-icons mr-3 alig-icon">account_circle</i>
	
  <div class="media-body">
    <h5 class="mt-0"><?php echo e($tellers->email); ?></h5>
   <p> Cras sit  <i class="material-icons">check_circle</i> </p>
   <span> teller</span>
  </div>
 
</div>
</div>
</div>
         
         </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
         </div>
                  </div>
         
         
      </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>