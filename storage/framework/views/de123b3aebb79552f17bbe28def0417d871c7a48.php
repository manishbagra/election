<?php $__env->startSection('title'); ?>
	Results
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-9">
      <div class="main-admin">
        <div class="head"> <h4 class="title-pop">FINALIZE ELECTION <div class="ser"><i class="material-icons">search</i></div></h4>
       
         </div>
        <!--// header section  close now //-->
        
        <div class="filer-tool">
          <ul class="list-inline btn-filt">
          <li class="list-inline-item dang"><a href="#"> <i class="material-icons">print</i> PRINT</a></li>
          </ul>
          <ul class="list-inline upload-tile-filt">
            <li class="list-inline-item"><a href="#"> Allready printed?</a></li>
            <li class="list-inline-item bg-lig-band" style="color:#f44336;"><a href="#" > REMOVE INFO </a></li>
          </ul>
        </div>
        <!-- // filter and tool section close  now //-->
       
        <div class="print-dit">   
               <ul class="list-inline">
         <h3>  People<span></span></h3>
        <li class="list-inline-item"><p><strong><?php echo e($people); ?></strong> People</p></li>
        <li class="list-inline-item"><p><strong><?php echo e($registerpeople); ?></strong> Registered People</p></li>
       <li class="list-inline-item"><p><strong><?php echo e($nonregisterpeople); ?></strong> Forbiden People</p></li>
       </ul>
           
               
        <ul class="list-inline">
        <h3>  Ballots<span></span></h3>
        <li class="list-inline-item"><p><strong><?php echo e($totalballot); ?></strong> Ballots</p></li>
        <li class="list-inline-item"><p><strong><?php echo e($validballot); ?></strong> Valid Ballots</p></li>
       <li class="list-inline-item"><p><strong><?php echo e($unvalidballot); ?></strong> Invalid Ballots</p></li>
       </ul>
            
        </div>
        
        <div class="table list-data">
          <table class="table table-hover">
            <thead> 
              <tr>
              <th scope="col">Rank</th>
                <th scope="col">Sure Name</th>
                <th scope="col">Last Name</th>
                <th scope="col"> Baha'i ID</th>
                <th scope="col">Community</th>
                <th scope="col">Nom. ballots</th>
                 </tr>
            </thead>
            <tbody>
              <tr>
              <td>1</td>
                <td> Michela</td>
                <td>Mark</td>
                <td>2324</td>
                <td>GOLD COAST CITY</td>
                <td>221</td>
                
              </tr>
              <tr class="bg-white">
               <td>2</td>
                <td>James</td>
                <td>0</td>
                <td>3434</td>
                <td>GOLD COAST CITY</td>
                 <td>205</td>
              </tr>
              <tr>
               <td>3</td>
                <td>Nttacsha</td>
                <td>0</td>
                <td>3434</td>
                <td>GOLD COAST CITY</td>
                 <td>213</td>
              </tr>
              <tr  class="bg-white">
               <td>4</td>
                <td>Ahmad</td>
                <td>Mark</td>
                <td>2323</td>
                <td>GOLD COAST CITY</td>
                 <td>261</td>
              </tr>
              <tr>
               <td>5</td>
                <td>Borhan</td>
                <td>Mark</td>
                <td>45</td>
                <td>GOLD COAST CITY</td>
                <td>61</td>
              </tr>
              <tr  class="bg-white">
               <td>6</td>
                <td>Sahba</td>
                <td>Mark</td>
                <td>654</td>
                <td>GOLD COAST CITY</td>
                <td>156</td>
              </tr>
              <tr>
               <td>7</td>
                <td>Michela</td>
                <td>Mark</td>
                <td>786</td>
                <td>GOLD COAST CITY</td>
                <td>121</td>
              </tr>
               
               
            </tbody>
          </table>
          <!--// table header section //  --> 
        </div>
      </div>
    </div>
	<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>