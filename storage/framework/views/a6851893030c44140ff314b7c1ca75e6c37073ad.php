<!DOCTYPE html>
<html lang="en" >
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content='<?php echo e(csrf_token()); ?>' />
<title><?php echo $__env->yieldContent('title'); ?></title>
<!-- Bootstrap CSS -->
 <?php echo Html::style('css/bootstrap.min.css'); ?>

 <?php echo Html::style('css/make-custome.css'); ?>


<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
</head>

<body class="bg-full" >
<div class="container-fluid admin">
  <div class="row">
   
       
     
        <!-- Left Menu Navigation -->
        <?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- /Left Menu Navigation -->
        
        <!-- Top Right Side Navigation -->
        <?php echo $__env->make('layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- /Top Right Side Navigation -->
        
        <!-- page content -->
        <?php echo $__env->yieldContent('content'); ?>
        <!-- /page content -->
		
  
	<!-- jQuery -->
	

	<?php echo Html::script('js/app.js'); ?>

    <?php echo Html::script('js/jquery3.2.1.min.js'); ?>

	<?php echo Html::script('js/popper.min.js'); ?>

	<?php echo Html::script('js/bootstrap.min.js'); ?>

	
	<script src="https://use.fontawesome.com/abf49d706d.js"></script>
	<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
    <?php echo Html::script('js/election_service.js'); ?>

    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	
	 </div>
</div>
  
   
	
</body>
</html>